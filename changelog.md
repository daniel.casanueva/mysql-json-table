## 0.1.4.0
* Add `Hashable` instance for the `Id` type.

## 0.1.3.0
* New instances for `Id`:
  - `FromJSONKey`
  - `ToJSONKey`
  - `FromHttpApiData`
  - `ToHttpApiData`

## 0.1.2.0
* Re-export: `ConnectInfo`, `defaultConnectInfo`, `Connection`.
* New function: `withSQL`.
* Fix id table creation.
* Fixes for id table queries.

## 0.1.1.0
* Add new function: `moveId`.

## 0.1.0.1
* Add lower bound for mysql-simple dependency.

## 0.1.0.0
* Initial release.
